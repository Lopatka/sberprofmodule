package com.sbercourses.library.SberProfModule.project.dto;

import com.sbercourses.library.SberProfModule.project.model.Role;
import com.sbercourses.library.SberProfModule.project.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class UserDTO extends CommonDTO {

    private Long id;
    private Long roleId;

    private String lastName;

    private String firstName;

    private String middleName;

    private String address;

    private String phone;

    private LocalDate birthDate;

    private String login;

    private String password;

    private String backUpEmail;

    public UserDTO(final User user) {
        this.id = user.getId();
        this.roleId = user.getRoleId();
        this.lastName = user.getLastName();
        this.middleName = user.getMiddleName();
        this.address = user.getAddress();
        this.phone = user.getPhone();
        this.birthDate = user.getBirthDate();
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.backUpEmail = user.getBackUpEmail();
    }
}
