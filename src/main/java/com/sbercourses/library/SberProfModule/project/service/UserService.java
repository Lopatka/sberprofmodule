package com.sbercourses.library.SberProfModule.project.service;


import com.sbercourses.library.SberProfModule.project.dto.BookDTO;
import com.sbercourses.library.SberProfModule.project.dto.OverduePublishDTO;
import com.sbercourses.library.SberProfModule.project.dto.UserDTO;
import com.sbercourses.library.SberProfModule.project.model.Publish;
import com.sbercourses.library.SberProfModule.project.model.Role;
import com.sbercourses.library.SberProfModule.project.model.User;
import com.sbercourses.library.SberProfModule.project.repository.BookRepository;
import com.sbercourses.library.SberProfModule.project.repository.PublishRepository;
import com.sbercourses.library.SberProfModule.project.repository.RoleRepository;
import com.sbercourses.library.SberProfModule.project.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService extends GenericService<User, UserDTO>{

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PublishRepository publishRepository;

    public UserService(UserRepository userRepository, RoleRepository roleRepository, PublishRepository publishRepository, BookRepository bookRepository){
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.publishRepository = publishRepository;
    }
    @Override
    public User update(User object) {
        Role role = roleRepository.findById(object.getRoleId())
                .orElseThrow(() -> new NotFoundException("Such role with id=" + object.getRoleId() + " was not found"));
        object.setRole(role);
        return userRepository.save(object);
    }

    @Override
    public User updateFromDTO(UserDTO object, Long userId) {
        User user = userRepository.findById(userId).orElseThrow(
                () -> new NotFoundException("User with such ID: " + userId + " not found"));
        user.setLastName(object.getLastName());
        user.setFirstName(object.getFirstName());
        user.setMiddleName(object.getMiddleName());
        user.setAddress(object.getAddress());
        user.setPhone(object.getPhone());
        user.setBirthDate(object.getBirthDate());
        user.setLogin(object.getLogin());
        user.setPassword(object.getPassword());
        user.setBackUpEmail(object.getBackUpEmail());
        Role role = roleRepository.findById(object.getRoleId())
                .orElseThrow(() -> new NotFoundException("Such role with id=" + object.getRoleId() + " was not found"));
        user.setRole(role);
        user.setRoleId(role.getId());
        return userRepository.save(user);
    }

    @Override
    public User createFromDTO(UserDTO newDtoObject) {
        User newUser = new User();
        newUser.setLastName(newDtoObject.getLastName());
        newUser.setFirstName(newDtoObject.getFirstName());
        newUser.setMiddleName(newDtoObject.getMiddleName());
        newUser.setAddress(newDtoObject.getAddress());
        newUser.setPhone(newDtoObject.getPhone());
        newUser.setBirthDate(newDtoObject.getBirthDate());
        newUser.setLogin(newDtoObject.getLogin());
        newUser.setPassword(newDtoObject.getPassword());
        newUser.setBackUpEmail(newDtoObject.getBackUpEmail());
        newUser.setCreatedBy(newDtoObject.getCreatedBy());
        newUser.setCreatedWhen(newDtoObject.getCreatedWhen());

        Role role = roleRepository.findById(newDtoObject.getRoleId())
                .orElseThrow(() -> new NotFoundException("Such role with id=" + newDtoObject.getRoleId() + " was not found"));
        newUser.setRole(role);
        newUser.setRoleId(role.getId());
        return userRepository.save(newUser);
    }

    @Override
    public User createFromEntity(User newObject) {
        Role role = roleRepository.findById(newObject.getRoleId())
                .orElseThrow(() -> new NotFoundException("Such role with id=" + newObject.getRoleId() + " was not found"));
        newObject.setRole(role);
        return userRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        User user = userRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("User with such ID: " + objectId + " not found"));
        userRepository.delete(user);
    }

    @Override
    public User getOne(Long objectId) {
        return userRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("User with such ID: " + objectId + " not found"));
    }

    @Override
    public List<User> listAll() {
        return userRepository.findAll();
    }

    public Set<OverduePublishDTO> getOverdueBooks(Long userId){
        User user = userRepository.findById(userId).orElseThrow(
                () -> new NotFoundException("User with such ID: " + userId + " not found"));
        Set<OverduePublishDTO> overduePublishDTOS = new HashSet<>();

        for(Publish publish : publishRepository.getOverduePublishByUserId(userId)){
            OverduePublishDTO overduePublishDTO = new OverduePublishDTO();
            BookDTO bookDTO = new BookDTO(publish.getBook());
            overduePublishDTO.setBookDTO(bookDTO);
            overduePublishDTO.setRentDate(publish.getRentDate());
            overduePublishDTO.setReturnDate(publish.getReturnDate());

            Period period = Period.between(publish.getReturnDate().toLocalDate(), LocalDateTime.now().toLocalDate());
            overduePublishDTO.setOverdueDays(period.getDays());

            overduePublishDTOS.add(overduePublishDTO);
        }
        return overduePublishDTOS;
    }
}
