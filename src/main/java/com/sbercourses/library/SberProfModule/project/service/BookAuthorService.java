package com.sbercourses.library.SberProfModule.project.service;

import com.sbercourses.library.SberProfModule.project.dto.AuthorDTO;
import com.sbercourses.library.SberProfModule.project.dto.BookAuthorDTO;
import com.sbercourses.library.SberProfModule.project.model.Author;
import com.sbercourses.library.SberProfModule.project.model.Book;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookAuthorService {

    private final BookService bookService;

    public BookAuthorService(BookService bookService) {
        this.bookService = bookService;
    }

    public List<BookAuthorDTO> getAllBooksWithAuthors() {
        List<Book> books = bookService.listAll();
        List<AuthorDTO> authorDTOS;
        List<BookAuthorDTO> bookAuthorDTOList = new ArrayList<>();

        for (Book book : books) {
            authorDTOS = new ArrayList<>();
            for (Author author : book.getAuthors()) {
                AuthorDTO authorDTO = new AuthorDTO(author);
                authorDTOS.add(authorDTO);
            }
            BookAuthorDTO bookAuthorDTO = new BookAuthorDTO(book, authorDTOS);
            bookAuthorDTOList.add(bookAuthorDTO);
        }
        return bookAuthorDTOList;
    }
}
