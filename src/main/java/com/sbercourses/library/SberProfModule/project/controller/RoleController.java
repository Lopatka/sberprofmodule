package com.sbercourses.library.SberProfModule.project.controller;

import com.sbercourses.library.SberProfModule.project.dto.RoleDTO;
import com.sbercourses.library.SberProfModule.project.model.Role;
import com.sbercourses.library.SberProfModule.project.service.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;


@RestController
@RequestMapping("/roles")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Роли",
        description = "Контроллер для работы с ролями нашей библиотеки.")
public class RoleController
        extends GenericController<Role> {

    private final GenericService<Role, RoleDTO> roleService;

    public RoleController(GenericService<Role, RoleDTO> roleService) {
        this.roleService = roleService;
    }

    @Operation(description = "Получить информацию о роли по ее ID",
            method = "getOne")
    @RequestMapping(value = "/getRole", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<Role> getOne(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(roleService.getOne(id));
    }

    @Operation(description = "Получить список всех ролей",
            method = "listAll")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Role>> listAll() {
        return ResponseEntity.status(HttpStatus.OK).body(roleService.listAll());
    }

    @Operation(description = "Добавить роль",
            method = "add")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Role> add(@RequestBody RoleDTO roleDTO) {
        roleDTO.setCreatedWhen(LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.CREATED).body(roleService.createFromDTO(roleDTO));
    }

    @Operation(description = "Обновить роль по ее ID",
            method = "update")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Role> update(@RequestBody RoleDTO roleDTO,
                                       @RequestParam(value = "roleId") Long roleId) {
        return ResponseEntity.status(HttpStatus.CREATED).body(roleService.updateFromDTO(roleDTO, roleId));
    }

    @Operation(description = "Удалить роль по ее ID",
            method = "delete")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "roleId") Long roleId) {
        roleService.delete(roleId);
        return ResponseEntity.status(HttpStatus.OK).body("Роль успешно удалена");
    }
}
