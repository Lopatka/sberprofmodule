package com.sbercourses.library.SberProfModule.project.service;

import com.sbercourses.library.SberProfModule.project.dto.AuthorDTO;
import com.sbercourses.library.SberProfModule.project.dto.BookAuthorDTO;
import com.sbercourses.library.SberProfModule.project.dto.BookDTO;
import com.sbercourses.library.SberProfModule.project.model.Author;
import com.sbercourses.library.SberProfModule.project.model.Book;
import com.sbercourses.library.SberProfModule.project.model.Genre;
import com.sbercourses.library.SberProfModule.project.repository.AuthorRepository;
import com.sbercourses.library.SberProfModule.project.repository.BookRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class BookService
        extends GenericService<Book, BookAuthorDTO> {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    public BookService(BookRepository bookRepository,
                       AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public Book update(Book object) {
        return bookRepository.save(object);
    }

    @Override
    public Book updateFromDTO(BookAuthorDTO object, Long bookId) {
        Book book = bookRepository.findById(bookId).orElseThrow(
                () -> new NotFoundException("Such book with id=" + bookId + " now found"));
        book.setTitle(object.getTitle());
        book.setAmount(object.getAmount());
        book.setGenre(object.getGenre());
        book.setPublishYear(object.getPublishYear());
        book.setStoragePlace(object.getStoragePlace());
        book.setOnlineCopy(object.getOnlineCopy());
        Set<Author> authors = new HashSet<>(authorRepository.findAllById(object.getAuthorIds()));

        //TODO: подумать что делать если хотим убрать автора из книги
        //TODO: создать автора "Без Автора" и выставлять его при удалении автора (если он единственный у книги).
        book.setAuthors(authors);
        return bookRepository.save(book);
    }

    @Override
    public Book createFromDTO(BookAuthorDTO newDtoObject) {
        Book book = new Book();
        book.setTitle(newDtoObject.getTitle());
        book.setAmount(newDtoObject.getAmount());
        book.setGenre(newDtoObject.getGenre());
        book.setOnlineCopy(newDtoObject.getOnlineCopy());
        book.setPublishYear(newDtoObject.getPublishYear());
        book.setStoragePlace(newDtoObject.getStoragePlace());
        book.setCreatedBy(newDtoObject.getCreatedBy());
        book.setCreatedWhen(newDtoObject.getCreatedWhen());

        Set<Author> authors = new HashSet<>(authorRepository.findAllById(newDtoObject.getAuthorIds()));

        book.setAuthors(authors);
        return bookRepository.save(book);
    }

    public Book createFromDTO(BookDTO bookDTO) {
        Book book = new Book();
        book.setTitle(bookDTO.getTitle());
        book.setAmount(bookDTO.getAmount());
        book.setGenre(bookDTO.getGenre());
        book.setOnlineCopy(bookDTO.getOnlineCopy());
        book.setPublishYear(bookDTO.getPublishYear());
        book.setStoragePlace(bookDTO.getStoragePlace());
        book.setCreatedBy(bookDTO.getCreatedBy());
        book.setCreatedWhen(bookDTO.getCreatedWhen());
        return bookRepository.save(book);
    }

    @Override
    public Book createFromEntity(Book newObject) {
        return bookRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        Book book = bookRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Such book with id=" + objectId + " now found"));
        bookRepository.delete(book);
    }

    @Override
    public Book getOne(Long objectId) {
        return bookRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Such book with id=" + objectId + " now found"));
    }

    @Override
    public List<Book> listAll() {
        return bookRepository.findAll();
    }

    public List<Book> search(String title, String authorFIO, Genre genre) {
        return bookRepository.search(title, authorFIO, genre);
    }
}

