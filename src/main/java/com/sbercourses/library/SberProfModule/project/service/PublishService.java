package com.sbercourses.library.SberProfModule.project.service;


import com.sbercourses.library.SberProfModule.project.dto.PublishDTO;
import com.sbercourses.library.SberProfModule.project.model.Book;
import com.sbercourses.library.SberProfModule.project.model.Publish;
import com.sbercourses.library.SberProfModule.project.model.User;
import com.sbercourses.library.SberProfModule.project.repository.BookRepository;
import com.sbercourses.library.SberProfModule.project.repository.PublishRepository;
import com.sbercourses.library.SberProfModule.project.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import java.util.List;

@Service
public class PublishService extends GenericService<Publish, PublishDTO>{
    private final PublishRepository publishRepository;

    private final UserRepository userRepository;

    private final BookRepository bookRepository;

    public PublishService(PublishRepository publishRepository, UserRepository userRepository,
                          BookRepository bookRepository) {
        this.publishRepository = publishRepository;
        this.userRepository = userRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public Publish update(Publish object) {
        return publishRepository.save(object);
    }

    @Override
    public Publish updateFromDTO(PublishDTO object, Long objectId) {
        Publish publish = publishRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Publishing with such ID: " + objectId + " not found"));
        publish.setRentDate(object.getRentDate());
        publish.setReturnDate(object.getReturnDate());
        publish.setRentPeriod(object.getRentPeriod());
        publish.setReturned(object.isReturned());

        return publishRepository.save(publish);
    }

    @Override
    public Publish createFromDTO(PublishDTO newDtoObject) {
        Publish newPublish = new Publish();
        newPublish.setRentDate(newDtoObject.getRentDate());
        newPublish.setReturnDate(newDtoObject.getReturnDate());
        newPublish.setRentPeriod(newDtoObject.getRentPeriod());
        newPublish.setReturned(newDtoObject.isReturned());
        newPublish.setCreatedBy(newDtoObject.getCreatedBy());
        newPublish.setCreatedWhen(newDtoObject.getCreatedWhen());

        User user = userRepository.findById(newDtoObject.getUserId())
                .orElseThrow(() -> new NotFoundException("Such user with id=" + newDtoObject.getUserId() + " was not found"));
        Book book = bookRepository.findById(newDtoObject.getBookId())
                .orElseThrow(() -> new NotFoundException("Such book with id=" + newDtoObject.getBookId() + " was not found"));

        newPublish.setUser(user);
        newPublish.setBook(book);

        return publishRepository.save(newPublish);
    }

    @Override
    public Publish createFromEntity(Publish newObject) {
        return publishRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        Publish publish = publishRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Publishing with such ID: " + objectId + " not found"));
        publishRepository.delete(publish);
    }

    @Override
    public Publish getOne(Long objectId) {
        return publishRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Publishing with such ID: " + objectId + " not found"));
    }

    @Override
    public List<Publish> listAll() {
        return publishRepository.findAll();
    }
}
