package com.sbercourses.library.SberProfModule.project.controller;

import com.sbercourses.library.SberProfModule.project.dto.PublishDTO;
import com.sbercourses.library.SberProfModule.project.model.Publish;
import com.sbercourses.library.SberProfModule.project.service.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/publish")
//CORS Filters
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Выдача",
        description = "Контроллер для работы с выдачей книг пользователям нашей библиотеки.")
public class PublishController extends GenericController<Publish>{

    private final GenericService<Publish, PublishDTO> publishService;

    public PublishController(GenericService<Publish, PublishDTO> publishService) {
        this.publishService = publishService;
    }

    @Operation(description = "Получить информацию о выдаче книги по ее ID",
            method = "getOne")
    @RequestMapping(value = "/getPublish", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    ResponseEntity<Publish> getOne(Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(publishService.getOne(id));
    }

    @Operation(description = "Получить список всех выдачах",
            method = "listAllPublications")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Publish>> listAllPublications() {
        return ResponseEntity.status(HttpStatus.OK).body(publishService.listAll());
    }

    @Operation(description = "Добавить новую выдачу",
            method = "add")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Publish> add(@RequestBody PublishDTO publishDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(publishService.createFromDTO(publishDTO));
    }

    @Operation(description = "Изменить информацию о выдаче книги по ее ID",
            method = "updatePublish")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Publish> updatePublish(@RequestBody PublishDTO publishDTO,
                                           @RequestParam(value = "publishId") Long publishId) {
        return ResponseEntity.status(HttpStatus.CREATED).body(publishService.updateFromDTO(publishDTO, publishId));
    }

    @Operation(description = "Удалить выдачу книги по ее ID",
            method = "delete")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "publishId") Long publishId) {
        publishService.delete(publishId);
        return ResponseEntity.status(HttpStatus.OK).body("Выдача успешно удалена");
    }
}
