package com.sbercourses.library.SberProfModule.project.dto;

import com.sbercourses.library.SberProfModule.project.model.RoleName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class RoleDTO
        extends CommonDTO {

    private Long id;

    private RoleName roleName;

    private String description;

}

