package com.sbercourses.library.SberProfModule.project.controller;

import com.sbercourses.library.SberProfModule.project.dto.AuthorDTO;
import com.sbercourses.library.SberProfModule.project.dto.BookAuthorDTO;
import com.sbercourses.library.SberProfModule.project.model.Author;
import com.sbercourses.library.SberProfModule.project.model.Book;
import com.sbercourses.library.SberProfModule.project.model.Genre;
import com.sbercourses.library.SberProfModule.project.service.BookService;
import com.sbercourses.library.SberProfModule.project.service.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/books")
//CORS Filters
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Книги",
        description = "Контроллер для работы с книгами нашей библиотеки.")
public class BookController {

    private final GenericService<Book, BookAuthorDTO> bookService;
    private final GenericService<Author, AuthorDTO> authorService;

    public BookController(GenericService<Book, BookAuthorDTO> bookService,
                          GenericService<Author, AuthorDTO> authorService) {
        this.bookService = bookService;
        this.authorService = authorService;
    }

    @Operation(description = "Получить информацию об одной книге по ее ID",
            method = "getOne")
    @RequestMapping(value = "/getBook", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> getOne(@RequestParam(value = "bookId") Long bookId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(bookService.getOne(bookId));
    }

    @Operation(description = "Получить список всех книг",
            method = "listAllBooks")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Book>> listAllBooks() {
        return ResponseEntity.status(HttpStatus.OK).body(bookService.listAll());
    }

    @Operation(description = "Добавить книгу",
            method = "add")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> add(@RequestBody BookAuthorDTO newBook) {
        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.createFromDTO(newBook));
    }

    @Operation(description = "Изменить информацию книги по ее ID",
            method = "updateBook")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> updateBook(@RequestBody BookAuthorDTO book,
                                           @RequestParam(value = "bookId") Long bookId) {
        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.updateFromDTO(book, bookId));
    }

    @Operation(description = "Удалить кингу по ее ID",
            method = "delete")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "bookId") Long bookId) {
        bookService.delete(bookId);
        return ResponseEntity.status(HttpStatus.OK).body("Книга успешно удалена");
    }

    @Operation(description = "Добавить автора книге",
            method = "addAuthor")
    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> addAuthor(@RequestParam(value = "bookId") Long bookId,
                                          @RequestParam(value = "authorId") Long authorId) {
        Author author = authorService.getOne(authorId);
        Book book = bookService.getOne(bookId);

        book.getAuthors().add(author);

        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.update(book));
    }

    @Operation(description = "Поиск книги по названию/автору/жанру", method = "search")
    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Book>> search(@RequestParam(value = "title", required = false) String title,
                                             @RequestParam(value = "authorFio", required = false) String authorFio,
                                             @RequestParam(value = "genre", required = false) Genre genre) {
        return ResponseEntity.status(HttpStatus.OK).body(((BookService) bookService).search(title, authorFio, genre));
    }
}

