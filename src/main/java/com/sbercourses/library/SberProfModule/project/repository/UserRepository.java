package com.sbercourses.library.SberProfModule.project.repository;


import com.sbercourses.library.SberProfModule.project.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select * from users as u where (select p.id from publishing as p ) = u.id)",
            nativeQuery = true)
    List<User> getAllUserWithBooks();
}
