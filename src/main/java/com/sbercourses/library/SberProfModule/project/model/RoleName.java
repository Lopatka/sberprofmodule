package com.sbercourses.library.SberProfModule.project.model;

public enum RoleName {
    USER,
    EMPLOYEE,
    ADMIN
}
