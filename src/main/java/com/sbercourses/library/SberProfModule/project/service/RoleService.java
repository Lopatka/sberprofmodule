package com.sbercourses.library.SberProfModule.project.service;

import com.sbercourses.library.SberProfModule.project.dto.RoleDTO;
import com.sbercourses.library.SberProfModule.project.model.Role;
import com.sbercourses.library.SberProfModule.project.repository.RoleRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class RoleService
        extends GenericService<Role, RoleDTO> {

    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role update(Role object) {
        return roleRepository.save(object);
    }

    @Override
    public Role updateFromDTO(RoleDTO object, Long roleId) {
        Role role = roleRepository.findById(roleId).orElseThrow(
                () -> new NotFoundException("Role with such ID: " + roleId + " not found"));
        role.setRoleName(object.getRoleName());
        role.setDescription(object.getDescription());
        return roleRepository.save(role);
    }

    @Override
    public Role createFromDTO(RoleDTO newDtoObject) {
        Role newRole = new Role();
        newRole.setRoleName(newDtoObject.getRoleName());
        newRole.setDescription(newDtoObject.getDescription());
        return roleRepository.save(newRole);
    }

    @Override
    public Role createFromEntity(Role newObject) {
        return roleRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        Role role = roleRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Role with such ID: " + objectId + " not found"));
        roleRepository.delete(role);
    }

    @Override
    public Role getOne(Long objectId) {
        return roleRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Role with such ID: " + objectId + " not found"));
    }

    @Override
    public List<Role> listAll() {
        return roleRepository.findAll();
    }
}
