package com.sbercourses.library.SberProfModule.project.repository;

import com.sbercourses.library.SberProfModule.project.model.Author;
import com.sbercourses.library.SberProfModule.project.model.Book;
import com.sbercourses.library.SberProfModule.project.model.Genre;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;



@Repository
public interface BookRepository
        extends JpaRepository<Book, Long> {

    Book findBookByTitleLike(String title);

    List<Book> findBooksByAuthors(Author author);

    @Query(value = "select distinct b.* from books b\n" +
            "left join books_authors ba \n" +
            "ON b.id = ba.author_id \n" +
            "left join authors a ON a.id = ba.book_id \n" +
            "where (a.author_fio like cast( :authorFIO AS text) \n" +
            "or b.title like cast( :title AS text)\n" +
            "or b.genre = cast( :#{#genre?.name()} AS text) \n" +
            ")", nativeQuery = true)
    List<Book> search(@Param("title") String title, @Param("authorFIO") String authorFIO, @Param("genre") Genre genre);
}

