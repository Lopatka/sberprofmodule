package com.sbercourses.library.SberProfModule.project.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sbercourses.library.SberProfModule.project.model.Book;
import com.sbercourses.library.SberProfModule.project.model.Publish;
import com.sbercourses.library.SberProfModule.project.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class PublishDTO extends CommonDTO {

    private Long id;

    private Long userId;

    private Long bookId;

    private LocalDateTime rentDate;

    private LocalDateTime returnDate;

    private boolean returned;

    private Integer rentPeriod;

    public PublishDTO(final Publish publish){
        this.id = publish.getId();
        this.userId = publish.getUser().getId();
        this.bookId = publish.getBook().getId();
        this.rentDate = publish.getRentDate();
        this.returnDate = publish.getReturnDate();
        this.returned = publish.isReturned();
        this.rentPeriod = publish.getRentPeriod();
    }
}
