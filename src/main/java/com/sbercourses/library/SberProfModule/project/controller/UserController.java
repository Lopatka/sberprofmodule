package com.sbercourses.library.SberProfModule.project.controller;

import com.sbercourses.library.SberProfModule.project.dto.OverduePublishDTO;
import com.sbercourses.library.SberProfModule.project.dto.UserDTO;
import com.sbercourses.library.SberProfModule.project.model.User;
import com.sbercourses.library.SberProfModule.project.service.GenericService;
import com.sbercourses.library.SberProfModule.project.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/users")
//CORS Filters
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Пользователи",
        description = "Контроллер для работы с пользователями нашей библиотеки.")
public class UserController extends GenericController<User>{

    private final GenericService<User, UserDTO> userService;

    public UserController(GenericService<User, UserDTO> userService){
        this.userService = userService;
    }

    @Operation(description = "Получить информацию об одном пользователе по его ID",
            method = "getOne")
    @RequestMapping(value = "/getUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    ResponseEntity<User> getOne(Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(userService.getOne(id));
    }

    @Operation(description = "Получить список всех пользователей",
            method = "listAllUsers")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> listAllUsers() {
        return ResponseEntity.status(HttpStatus.OK).body(userService.listAll());
    }

    @Operation(description = "Добавить нового пользователя",
            method = "add")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> add(@RequestBody UserDTO newUser) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createFromDTO(newUser));
    }

    @Operation(description = "Изменить информацию о пользователе по его ID",
            method = "updateUser")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(@RequestBody UserDTO user,
                                             @RequestParam(value = "userId") Long userId) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.updateFromDTO(user, userId));
    }

    @Operation(description = "Удалить пользователя по его ID",
            method = "delete")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "userId") Long userId) {
        userService.delete(userId);
        return ResponseEntity.status(HttpStatus.OK).body("Пользователь успешно удален");
    }

    @Operation(description = "Получить список несданных книг пользователя по его ID",
            method = "getOverdueBooks")
    @RequestMapping(value = "/getOverdueBooks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<OverduePublishDTO>> getOverdueBooks(@RequestParam(value = "userId") Long userId) {
        return ResponseEntity.status(HttpStatus.OK).body(((UserService) userService).getOverdueBooks(userId));
    }
}
