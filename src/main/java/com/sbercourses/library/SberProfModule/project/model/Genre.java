package com.sbercourses.library.SberProfModule.project.model;

public enum Genre {
    FANTASY("Фантастика"),
    SCIENCE_FICTION("Научная фантастика"),
    DRAMA("Драма"),
    NOVEL("Роман");

    private final String genreName;

    Genre(String genreName){
        this.genreName = genreName;
    }

    public String getGenreName(){
        return this.genreName;
    }
}

