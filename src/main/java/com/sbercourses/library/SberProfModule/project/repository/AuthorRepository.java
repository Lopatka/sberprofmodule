package com.sbercourses.library.SberProfModule.project.repository;

import com.sbercourses.library.SberProfModule.project.model.Author;
import com.sbercourses.library.SberProfModule.project.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface AuthorRepository
        extends JpaRepository<Author, Long> {
    List<Author> findAuthorByBooksIn(Set<Book> books);

    List<Author> findAuthorByBooks(Book book);

    List<Author> findAuthorByBooksId(Long bookId);

    List<Author> findAuthorByAuthorFIOLike(String authorFIO);

    @Query(value = "select b.id \n" +
            "from books_authors ba join books b ON ba.book_id = b.id \n" +
            "join authors a ON ba.author_id = a.id \n" +
            "where b.id in (select c.book_id\n" +
            "                    from books_authors c\n" +
            "                    group by c.book_id\n" +
            "                    having count(c.book_id) = 1) and a.id = :authorId", nativeQuery = true)
    List<Long> findBooksOnlyThisAuthorById(@Param("authorId") Long authorId);
}

