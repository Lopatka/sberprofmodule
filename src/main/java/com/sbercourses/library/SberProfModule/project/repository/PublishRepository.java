package com.sbercourses.library.SberProfModule.project.repository;

import com.sbercourses.library.SberProfModule.project.model.Publish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface PublishRepository extends JpaRepository<Publish, Long> {

    Publish getPublishByUserId(Long userId);

    @Query(value = "select * from publishing p \n" +
            "left join \n" +
            "books b ON p.book_id  = b.id\n" +
            "where p.user_id=:userId and p.returned=false\n" +
            "and p.rent_date + make_interval(days => p.rent_period) < now()", nativeQuery = true)
    Set<Publish> getOverduePublishByUserId(@Param("userId") Long userId);

    @Query(value = "select p.*  from publishing p \n" +
            "left join books b ON p.book_id = b.id\n" +
            "left join books_authors ba ON b.id = ba.book_id \n" +
            "left join authors a ON a.id = ba.author_id\n" +
            "where a.id = :authorId", nativeQuery = true)
    List<Publish> getAuthorPublishById(@Param("authorId") Long authorId);
}

