package com.sbercourses.library.SberProfModule.project.service;

import com.sbercourses.library.SberProfModule.project.dto.AuthorDTO;
import com.sbercourses.library.SberProfModule.project.dto.BookDTO;
import com.sbercourses.library.SberProfModule.project.model.Author;
import com.sbercourses.library.SberProfModule.project.model.Book;
import com.sbercourses.library.SberProfModule.project.model.Publish;
import com.sbercourses.library.SberProfModule.project.repository.AuthorRepository;
import com.sbercourses.library.SberProfModule.project.repository.BookRepository;
import com.sbercourses.library.SberProfModule.project.repository.PublishRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
public class AuthorService
        extends GenericService<Author, AuthorDTO> {

    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    private final PublishRepository publishRepository;


    public AuthorService(AuthorRepository authorRepository,
                         BookRepository bookRepository, PublishRepository publishRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publishRepository = publishRepository;
    }

    @Override
    public Author update(Author object) {
        return authorRepository.save(object);
    }

    @Override
    public Author updateFromDTO(AuthorDTO object, Long authorId) {
        Author author = authorRepository.findById(authorId).orElseThrow(
                () -> new NotFoundException("Author with such ID: " + authorId + " not found"));
        author.setAuthorFIO(object.getAuthorFIO());
        author.setLifePeriod(object.getLifePeriod());
        author.setDescription(object.getDescription());
        return authorRepository.save(author);
    }

    @Override
    public Author createFromDTO(AuthorDTO newDtoObject) {
        Author newAuthor = new Author();
        newAuthor.setAuthorFIO(newDtoObject.getAuthorFIO());
        newAuthor.setDescription(newDtoObject.getDescription());
        newAuthor.setLifePeriod(newDtoObject.getLifePeriod());
        newAuthor.setCreatedBy(newDtoObject.getCreatedBy());
        newAuthor.setCreatedWhen(newDtoObject.getCreatedWhen());
        return authorRepository.save(newAuthor);
    }

    @Override
    public Author createFromEntity(Author newObject) {
        return authorRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        Author author = authorRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Author with such ID: " + objectId + " not found"));
        List<Publish> publishes = publishRepository.getAuthorPublishById(objectId);

        for (Publish publish : publishes) {
            if (!publish.isReturned()) {
                throw new IllegalArgumentException("Author's book with such ID:" + publish.getBook().getId() + " has not been returned");
            }
        }
        List<Long> booksIds = authorRepository.findBooksOnlyThisAuthorById(objectId);

        for (Book book : author.getBooks()) {
            book.getAuthors().remove(author);
            if (booksIds.contains(book.getId())) {
                bookRepository.deleteById(book.getId());
            }
        }
        author.setBooks(new HashSet<>());
        bookRepository.deleteAllById(booksIds);
        authorRepository.delete(author);
    }

    @Override
    public Author getOne(Long objectId) {
        return authorRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Author with such ID: " + objectId + " not found"));
    }

    @Override
    public List<Author> listAll() {
        return authorRepository.findAll();
    }

    /**
     * Ищем все книги заданного автора.
     *
     * @param authorId Айди автора
     * @return List<Book> список книг автора
     */
    public List<BookDTO> getAllAuthorBooks(Long authorId) {
        Author author = authorRepository.findById(authorId).orElseThrow(
                () -> new NotFoundException("Author with such ID: " + authorId + " not found"));
        List<BookDTO> bookDTOList = new ArrayList<>();
        for (Book book : bookRepository.findBooksByAuthors(author)) {
            BookDTO bookDTO = new BookDTO(book);
            bookDTOList.add(bookDTO);
        }
        return bookDTOList;
    }
}

