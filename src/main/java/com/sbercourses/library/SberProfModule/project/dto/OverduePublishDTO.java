package com.sbercourses.library.SberProfModule.project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class OverduePublishDTO {

    private BookDTO bookDTO;

    private LocalDateTime returnDate;

    private LocalDateTime rentDate;

    private Integer overdueDays;
}
