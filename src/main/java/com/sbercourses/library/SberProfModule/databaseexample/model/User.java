package com.sbercourses.library.SberProfModule.databaseexample.model;

import lombok.*;

import java.time.LocalDateTime;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {
    //@Setter(AccessLevel.NONE)
    private Integer id;
    private String lastName;
    private String firstName;
    private LocalDateTime birthDate;
    private String phone;
    private String email;
    private String[] listBooks;
}
