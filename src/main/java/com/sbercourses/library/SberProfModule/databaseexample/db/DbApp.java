package com.sbercourses.library.SberProfModule.databaseexample.db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.sbercourses.library.SberProfModule.databaseexample.consts.DBConsts.*;


public enum DbApp {
    INSTANCE;
    private Connection connection;

    public Connection newConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
                USER,
                PASSWORD);
    }
}
