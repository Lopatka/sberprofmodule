package com.sbercourses.library.SberProfModule.databaseexample.dao;

import com.sbercourses.library.SberProfModule.databaseexample.model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BookDAO2 {

    private Connection connection;

    public BookDAO2(Connection connection) {
        this.connection = connection;
    }

    public Book findBookById(final Integer id) throws SQLException {
        PreparedStatement query = connection.prepareStatement("select * from books where id = ?");
        query.setInt(1, id);
        ResultSet resultSet = query.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book.setId(resultSet.getInt("id"));
            book.setAuthor(resultSet.getString("title"));
            book.setTitle(resultSet.getString("author"));
            book.setDate(resultSet.getString("date_added"));
            System.out.println(book);
        }
        return book;
    }

    public List<Book> findAllByTitles(final List<String> titles) throws SQLException {
        String sql = String.format("select * from books where title in (%s)",
                titles.stream()
                        .map(v -> "?")
                        .collect(Collectors.joining(", ")));
        List<Book> bookList = new ArrayList<>();
        Book book;
        PreparedStatement query = connection.prepareStatement(sql);
        int parametrIndex = 1;

        for (String title : titles) {
            query.setString(parametrIndex, title);
            parametrIndex++;
        }
        ResultSet resultSet = query.executeQuery();
        while (resultSet.next()) {
            book = new Book();
            book.setId(resultSet.getInt("id"));
            book.setAuthor(resultSet.getString("title"));
            book.setTitle(resultSet.getString("author"));
            book.setDate(resultSet.getString("date_added"));
            bookList.add(book);
        }
        return bookList;
    }
}

