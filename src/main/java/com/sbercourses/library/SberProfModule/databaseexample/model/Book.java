package com.sbercourses.library.SberProfModule.databaseexample.model;

import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    //@Setter(AccessLevel.NONE)
    private Integer id;
    private String title;
    private String author;
    private String date;
}
