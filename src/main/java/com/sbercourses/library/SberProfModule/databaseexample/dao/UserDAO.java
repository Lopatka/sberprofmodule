package com.sbercourses.library.SberProfModule.databaseexample.dao;

import com.sbercourses.library.SberProfModule.databaseexample.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserDAO {

    private Connection connection;

    public UserDAO(Connection connection) {
        this.connection = connection;
    }

    public int addUser(final User user) throws SQLException {
        int result;
        PreparedStatement query = connection.prepareStatement(
                "insert into users(last_name, first_name, birth_date, phone, email, list_books)\n" +
                        "values (?, ?, ?, ?, ?, ?);");
        query.setString(1, user.getLastName());
        query.setString(2, user.getFirstName());
        query.setTimestamp(3, Timestamp.valueOf(user.getBirthDate()));
        query.setString(4, user.getPhone());
        query.setString(5, user.getEmail());
        query.setArray(6, connection.createArrayOf("text", user.getListBooks()));
        result = query.executeUpdate();
        return result;
    }

    public List<String> findUserBookTitlesByPhone(final String phone) throws SQLException {
        List<String> bookTitlesList = new ArrayList<>();
        PreparedStatement query = connection.prepareStatement("select list_books from users where phone = ?");
        query.setString(1, phone);
        ResultSet resultSet = query.executeQuery();
        while (resultSet.next()) {
            bookTitlesList = Arrays.asList((String[]) resultSet.getArray("list_books").getArray());
        }
        return bookTitlesList;
    }
}
