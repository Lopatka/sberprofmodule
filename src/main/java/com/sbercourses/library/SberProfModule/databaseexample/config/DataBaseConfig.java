package com.sbercourses.library.SberProfModule.databaseexample.config;

import com.sbercourses.library.SberProfModule.databaseexample.dao.BookDAO2;
import com.sbercourses.library.SberProfModule.databaseexample.dao.UserDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.sbercourses.library.SberProfModule.databaseexample.consts.DBConsts.*;

@Configuration
public class DataBaseConfig {

    @Bean
    @Scope("singleton")
    //@Scope("prototype") etc.
    public Connection connection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
                USER,
                PASSWORD);
    }

    @Bean
    public BookDAO2 bookDAO2() throws SQLException {
        return new BookDAO2(connection());
    }

    @Bean
    @Scope("prototype")
    public UserDAO userDAO() throws SQLException {
        return new UserDAO(connection());
    }
}

