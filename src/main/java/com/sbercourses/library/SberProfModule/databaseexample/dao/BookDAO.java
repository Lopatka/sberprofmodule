package com.sbercourses.library.SberProfModule.databaseexample.dao;

import com.sbercourses.library.SberProfModule.databaseexample.db.DbApp;
import com.sbercourses.library.SberProfModule.databaseexample.model.Book;

import java.sql.*;

public class BookDAO {
    public Book findById(final Integer id) throws SQLException {
        try (Connection connection = DbApp.INSTANCE.newConnection()) {
            if (connection != null) {
                System.out.println("Connected to the DB");
            }
            else {
                System.out.println("Connection to DB failed");
            }
            PreparedStatement selectQuery = connection.prepareStatement("select * from books where id = ?");
            selectQuery.setInt(1, id);
            ResultSet resultSet = selectQuery.executeQuery();
            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt("id"));
                book.setTitle(resultSet.getString("title"));
                book.setAuthor(resultSet.getString("author"));
                book.setDate(resultSet.getString("date_added"));
                System.out.println(book);
                return book;
            }
        }
        return null;
    }

    public Book findBookByTitle(final String title) throws SQLException {
        try (Connection connection = DbApp.INSTANCE.newConnection()) {
            ///sql for getting data from DB by title
        }
        return null;
    }


    /*private Connection newConnection() throws SQLException {
        return DriverManager.getConnection(
             "jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
             USER, PASSWORD);
    }*/

}
