package com.sbercourses.library.SberProfModule.additional;

import org.springframework.data.relational.core.sql.In;

import java.util.Scanner;

public class Additional1 {
    public static void main(String[] args) {
        int[] test = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 153, 370, 371, 407, 1634, 8208, 9474,
                54748, 92727, 93084, 548834, 1741725, 4210818, 9800817, 9926315, 24678050};
        boolean testStatus = true;
        Scanner scanner = new Scanner(System.in);
        int inputNumber;

        for (int j : test) {
            if (!isArmstrongNumber(j)) {
                System.out.println("Число " + j + " не является числом Армстронга");
                testStatus = false;
            }
        }
        if (testStatus) {
            System.out.println("Все тесты прошли, успех!");
        }
        System.out.println("Проверить свое число: ");
        inputNumber = scanner.nextInt();
        if (!isArmstrongNumber(inputNumber)) {
            System.out.println("Число " + inputNumber + " не является числом Армстронга");
        } else {
            System.out.println("Число " + inputNumber + " является числом Армстронга");
        }
    }

    public static boolean isArmstrongNumber(int number) {
        int temp = number;
        int sum = 0;
        int length = String.valueOf(number).length();

        while (temp != 0) {
            sum += Math.pow(temp % 10, length);
            temp = temp / 10;
        }
        return number == sum;
    }
}
