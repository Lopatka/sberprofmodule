package com.sbercourses.library.SberProfModule.additional;

import java.math.BigInteger;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Additional2 {
    public static void main(String[] args) {
        int[] test = new int[]{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43,
                47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 571, 853};
        boolean testStatus = true;
        Scanner scanner = new Scanner(System.in);
        int inputNumber;

        for (int j : test) {
            if (!isPrimeV1(j) && !isPrimeV2(j)) {
                System.out.println("Число " + j + " не является простым числом");
                testStatus = false;
            }
        }
        if (testStatus) {
            System.out.println("Все тесты прошли, успех!");
        }
        System.out.println("Проверить свое число: ");
        inputNumber = scanner.nextInt();
        if (!isPrimeV1(inputNumber) && !isPrimeV2(inputNumber)) {
            System.out.println("Число " + inputNumber + " не является простым числом");
        } else {
            System.out.println("Число " + inputNumber + " является простым числом");
        }
    }

    public static boolean isPrimeV1(int number) {
        BigInteger bigInteger = BigInteger.valueOf(number);
        return bigInteger.isProbablePrime((int) Math.log(number));
    }

    public static boolean isPrimeV2(int number) {
        return number > 1
                && IntStream.rangeClosed(2, (int) Math.sqrt(number))
                .noneMatch(n -> (number % n == 0));
    }
}
