package com.sbercourses.library.SberProfModule;

import com.sbercourses.library.SberProfModule.databaseexample.config.DataBaseConfig;
import com.sbercourses.library.SberProfModule.databaseexample.dao.BookDAO2;
import com.sbercourses.library.SberProfModule.databaseexample.dao.UserDAO;
import com.sbercourses.library.SberProfModule.databaseexample.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
public class SberProfModuleApplication implements CommandLineRunner {

	private NamedParameterJdbcTemplate jdbcTemplate;

	public SberProfModuleApplication(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	public static void main(String[] args) {
		SpringApplication.run(SberProfModuleApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}