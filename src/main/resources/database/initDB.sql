create table if not exists books (
                       id serial primary key,
                       title varchar(30) not null,
                       author varchar(100) not null,
                       date_added timestamp not null
);
create table if not exists users (
                       id serial primary key,
                       last_name varchar(30) not null,
                       first_name varchar(30) not null,
                       birth_date timestamp not null,
                       phone varchar(16) not null,
                       email varchar(50) not null,
                       list_books text[] not null
)